/*
@Trigger Name  : SL_DialysisActivityTrigger
@AssignmentName: RHMP2-61
@Created by    : Vikash Agarwal
@Modified by   : 
@Description   : Trigger on lead which will be fired on after update operation.
*/
trigger SL_DialysisActivityTrigger on Dialysis_Activity__c (before insert, before update) {

    SL_Trigger.dispatchHandler(Dialysis_Activity__c.sObjectType, new SL_DialysisActivityTriggerHandler());
}