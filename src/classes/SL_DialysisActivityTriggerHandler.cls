/**
@ClassName    : SL_DialysisActivityTriggerHandler 
@JIRATicket   : RHMP2-61
@CreatedOn    : 12/Oct/2018
@ModifiedBy   : Vikash Agarwal
@Description  : This is the handler class for Dialysis_Activity__c trigger(SL_DialysisActivityTrigger).
                1) On inserting Dialysis_Activity__c record
                    - Update Max treatments allowed for month field on dialysis record
                2) On inserting and updating records
                    - Populate Account Details
                    - Populate Dialysis Clinic and field updates from Market Scorecard
					- Populate number of days in a month for date of service
*/
public with sharing class SL_DialysisActivityTriggerHandler extends SL_Trigger.baseHandler {

    /**
    @MethodName : beforeInsert
    @Param      : List<SObject> newList
    @Description: 
    **/
    public override void beforeInsert(List<SObject> newList) {
        //Set Account details on DA when Patient_Account__c not null
        setAccountDetails(NULL, (List<Dialysis_Activity__c>)newList);

        //Update Dialysis Clinic
        setDialysisClinicAndFields(NULL, (List<Dialysis_Activity__c>)newList);
        
        //Set Days in Month for Date of Service
        populateDaysInMonthForDateofService(NULL, (List<Dialysis_Activity__c>)newList);
        
        //Populate max treatment allowed for month
        updateTreatmentAllowedForMonths(NULL, (List<Dialysis_Activity__c>)newList); 
    }

    /**
    @MethodName : beforeUpdate
    @Param      : Map<Id, SObject> oldMap, Map<Id, SObject> newMap
    @Description: 
    **/
    public override void beforeUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        //Set Days in Month for Date of Service
        populateDaysInMonthForDateofService((Map<Id, Dialysis_Activity__c>)oldMap, (List<Dialysis_Activity__c>)newMap.values());
        
        //Set Account details on DA when Patient_Account__c not null
        setAccountDetails((Map<Id, Dialysis_Activity__c>)oldMap, (List<Dialysis_Activity__c>)newMap.values());

        //Update Dialysis Clinic
        setDialysisClinicAndFields((Map<Id, Dialysis_Activity__c>)oldMap, (List<Dialysis_Activity__c>)newMap.values());
        
        //Populate max treatment allowed for month
        updateTreatmentAllowedForMonths((Map<Id, Dialysis_Activity__c>)oldMap, (List<Dialysis_Activity__c>)newMap.values());
    }
    
    /**
    @MethodName : populateDaysInMonthForDateofService
    @Param      : Map<Id, Dialysis_Activity__c> oldMap, List<Dialysis_Activity__c> lstNew
    @Description: Populate days in month for Date of Service
    **/
    private void populateDaysInMonthForDateofService(Map<Id, Dialysis_Activity__c> oldMap, List<Dialysis_Activity__c> lstNew) {
        for(Dialysis_Activity__c objDA : lstNew) {
            if((oldMap == NULL || objDA.Date_of_Service__c != oldMap.get(objDA.Id).Date_of_Service__c) && objDA.Date_of_Service__c != NULL) {
                   Date dateOfService = objDA.Date_of_Service__c;
                   objDA.Days_in_Month__c = Date.daysInMonth(dateOfService.year(), dateOfService.month());
               }
        }
    }

    /**
    @MethodName : setDialysisClinicAndFields
    @Param      : Map<Id, Dialysis_Activity__c> oldMap, List<Dialysis_Activity__c> lstNew
    @Description: Update efficiency if market and set dialysis clinic and fields
    **/
    private void setDialysisClinicAndFields(Map<Id, Dialysis_Activity__c> oldMap, List<Dialysis_Activity__c> lstNew) {
        Set<String> setStrShippingStreet = new Set<String>();
        Set<String> setStrShippingCity = new Set<String>();
        Map<String, List<Dialysis_Activity__c>> mapClinicIdToDA = new Map<String, List<Dialysis_Activity__c>>();
        Map<Id, Id> mapPatientIdToClinicId = new Map<Id, Id>();

        for(Dialysis_Activity__c objDA : lstNew) {
            if( oldMap == NULL || (objDA.Dialysis_Clinic__c != oldMap.get(objDA.Id).Dialysis_Clinic__c || 
                                        (objDA.Dialysis_Clinic__c == NULL && 
                                            (objDA.Dialysis_Provider_Address_Line_1__c != oldMap.get(objDA.Id).Dialysis_Provider_Address_Line_1__c || 
                                             objDA.Dialysis_Provider_City__c != oldMap.get(objDA.Id).Dialysis_Provider_City__c)
                                        ) || 
                                        (objDA.Star_Rating__c == NULL || objDA.Efficiency_Rating__c == NULL || objDA.Savings_per_visit__c == NULL) 
                                    ) ) {
                    
                    if(objDA.Dialysis_Clinic__c == NULL && (objDA.Dialysis_Provider_Address_Line_1__c != NULL &&
                                                            objDA.Dialysis_Provider_City__c != NULL) ) {
                        setStrShippingStreet.add(objDA.Dialysis_Provider_Address_Line_1__c);
                        setStrShippingCity.add(objDA.Dialysis_Provider_City__c);
                    }
            }
        }

        Id idDialysisAccountRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Dialysis_Center' AND sObjectType = 'Account'].Id;
        //Map<String, Account> mapStreetAndCityToClinic = new Map<String, Account>();
        List<Account> lstAccounts = new List<Account>();
        for(Account a : [SELECT Id, RecordTypeId, ShippingCity, ShippingStreet, Five_Star__c
                         FROM Account
                         WHERE (/*ShippingStreet IN :setStrShippingStreet
                              AND*/ ShippingCity IN :setStrShippingCity
                              AND RecordTypeId = :idDialysisAccountRT)]) {
			lstAccounts.add(a);
            //mapStreetAndCityToClinic.put(a.ShippingStreet + '$$' + a.ShippingCity, a);
        }

        for(Dialysis_Activity__c objDA : lstNew) {
            if( oldMap == NULL || (objDA.Dialysis_Clinic__c != oldMap.get(objDA.Id).Dialysis_Clinic__c || 
                                        (objDA.Dialysis_Clinic__c == NULL && 
                                            (objDA.Dialysis_Provider_Address_Line_1__c != oldMap.get(objDA.Id).Dialysis_Provider_Address_Line_1__c || 
                                             objDA.Dialysis_Provider_City__c != oldMap.get(objDA.Id).Dialysis_Provider_City__c)
                                        ) || 
                                        (objDA.Star_Rating__c == NULL || objDA.Efficiency_Rating__c == NULL || objDA.Savings_per_visit__c == NULL) 
                                    ) ) {
                    
                    if(objDA.Dialysis_Clinic__c == NULL && (objDA.Dialysis_Provider_Address_Line_1__c != NULL &&
                                                            objDA.Dialysis_Provider_City__c != NULL) ) {
						
                        String strClinicId = fetchAccountBasedOnAddress(objDA, lstAccounts);
                        if(String.isNotBlank(strClinicId)) {
                            objDA.Dialysis_Clinic__c = strClinicId;

                            if(mapClinicIdToDA.containsKey(strClinicId)) {
                                mapClinicIdToDA.get(strClinicId).add(objDA);
                            }
                            else {
                                mapClinicIdToDA.put(strClinicId, new List<Dialysis_Activity__c>{objDA});
                            }
                            mapPatientIdToClinicId.put(objDA.Patient__c, strClinicId);
                        }
                    }
                    else {
                        if(mapClinicIdToDA.containsKey(objDA.Dialysis_Clinic__c)) {
                            mapClinicIdToDA.get(objDA.Dialysis_Clinic__c).add(objDA);
                        }
                        else {
                            mapClinicIdToDA.put(objDA.Dialysis_Clinic__c, new List<Dialysis_Activity__c>{objDA});
                        }
                        mapPatientIdToClinicId.put(objDA.Patient__c, objDA.Dialysis_Clinic__c);
                    }
            }
        }

        if(!mapClinicIdToDA.isEmpty() && !mapPatientIdToClinicId.isEmpty())
            getScorecardAndFieldUpdates(mapClinicIdToDA, mapPatientIdToClinicId);
    }
    
    private String fetchAccountBasedOnAddress(Dialysis_Activity__c objDA, List<Account> lstAccounts) {
        for(Account objAccount : lstAccounts) {
            if(objAccount.ShippingCity == objDA.Dialysis_Provider_City__c) {
                if(objAccount.ShippingStreet.containsIgnoreCase( objDA.Dialysis_Provider_Address_Line_1__c ) )
                    return objAccount.Id;
            }
        }
        return null;
    }

    /**
    @MethodName : getScorecardAndFieldUpdates
    @Param      : Map<String, List<Dialysis_Activity__c>> mapClinicIdToDA, Map<Id, Id> mapPatientIdToClinicId
    @Description: 
    **/
    private void getScorecardAndFieldUpdates(Map<String, List<Dialysis_Activity__c>> mapClinicIdToDA, Map<Id, Id> mapPatientIdToClinicId) {
        Map<Id, Id> mapClinicIdToMarketId = new Map<Id, Id>();
        if(!mapPatientIdToClinicId.isEmpty()) {
            for(Contact a : [SELECT Id, Market__c FROM Contact WHERE Id IN :mapPatientIdToClinicId.keySet()]) {
                String clinicId = mapPatientIdToClinicId.get(a.Id);
                mapClinicIdToMarketId.put(clinicId, a.Market__c);
            }
        }

        for(Market_Scorecard__c objMSC : [SELECT Id, Score__c, Savings__c, Market__c, Dialysis_Center_Location__c, Dialysis_Center_Location__r.Five_Star__c
                                          FROM Market_Scorecard__c
                                          WHERE Market__c IN :mapClinicIdToMarketId.values()
                                          AND Dialysis_Center_Location__c IN :mapClinicIdToDA.keySet()]) {
            for(Dialysis_Activity__c objDA : mapClinicIdToDA.get(objMSC.Dialysis_Center_Location__c)) {
                objDA.Efficiency_Rating__c = objMSC.Score__c;
                objDA.Savings_per_visit__c = objMSC.Savings__c;
                objDA.Star_Rating__c = objMSC.Dialysis_Center_Location__r.Five_Star__c;
            }
        }
    }

    /**
    @MethodName : setAccountDetails
    @Param      : Map<Id, Dialysis_Activity__c> oldMap, List<Dialysis_Activity__c> lstNew
    @Description: Set account details
    **/
    private void setAccountDetails(Map<Id, Dialysis_Activity__c> oldMap, List<Dialysis_Activity__c> lstNew) {
        Set<Id> setPatientIds = new Set<Id>();
        List<Dialysis_Activity__c> lstFilteredDAs = new List<Dialysis_Activity__c>();

        for(Dialysis_Activity__c objDA : lstNew) {
            if((oldMap == NULL || objDA.Patient_Account__c != oldMap.get(objDA.Id).Patient_Account__c) 
                && objDA.Patient_Account__c == NULL && objDA.Patient__c != NULL) {
                setPatientIds.add(objDA.Patient__c);
                lstFilteredDAs.add(objDA);
            }
        }

        Map<Id, Contact> mapContacts = new Map<Id, Contact>();
        for(Contact objCon : [SELECT Id, Primary_Access_Type__c, Modality__c, AccountId, Account.Primary_Physician_Office__c,
                                     Account.Primary_Physician_Name__c
                              FROM Contact
                              WHERE Id IN :setPatientIds]) {
            mapContacts.put(objCon.Id, objCon);
        }

        for(Dialysis_Activity__c objDA : lstFilteredDAs) {
            Contact c = mapContacts.get(objDA.Patient__c);
            objDA.Access_Type__c = c.Primary_Access_Type__c;
            objDA.Patient_Account__c = c.AccountId;
            objDA.Physician_Office__c = c.Account.Primary_Physician_Office__c;
            objDA.Physician__c = c.Account.Primary_Physician_Name__c;
            
            if(String.isBlank(objDA.Modality__c))
                objDA.Modality__c = c.Modality__c;
        }
    }
    
    /**
    @MethodName : updateTreatmentAllowedForMonths
    @Param      : Map<Id, Dialysis_Activity__c> newMapDialysis
    @Description: Populate Max_Treatments_Allowed_for_Month__c on dialysis activity:
					1) On insert and
					2) On update of Date of Service on Dialysis Activity
    **/
    private void updateTreatmentAllowedForMonths(Map<Id, Dialysis_Activity__c> oldMap, List<Dialysis_Activity__c> lstDialysis) {
        List<Dialysis_Activity__c> lstFilteredDAs = new List<Dialysis_Activity__c>();
        Set<Id> setPatientIds = new Set<Id>();
        
        for(Dialysis_Activity__c objDialysis : lstDialysis) {
            if((oldMap == NULL || objDialysis.Date_of_Service__c != oldMap.get(objDialysis.Id).Date_of_Service__c) && objDialysis.Date_of_Service__c != NULL) {
                lstFilteredDAs.add(objDialysis);
                setPatientIds.add(objDialysis.Patient__c);
			}
        }
        
        Map<Id, String> mapPatientIdToDialySchedule = new Map<Id, String>();
        for(Contact patient : [SELECT Id, Dialysis_Schedule__c FROM Contact WHERE Id IN:setPatientIds]) {
			mapPatientIdToDialySchedule.put(patient.Id, patient.Dialysis_Schedule__c);
		}
        
        for(Dialysis_Activity__c objDA : lstFilteredDAs) {
            String strDialysisSchedule = String.valueOf(mapPatientIdToDialySchedule.get(objDA.Patient__c));
            Date dateOfService = objDA.Date_of_Service__c;
            Integer intTreatmentCount = 0;
            Integer intMWFTreatmentCount = getTreatmentCounts(dateOfService.toStartOfMonth(), 'M W F');
            Integer intTThSTreatmentCount = getTreatmentCounts(dateOfService.toStartOfMonth(), 'T Th S');
            
            if(strDialysisSchedule == 'M W F')
                intTreatmentCount = intMWFTreatmentCount;
            else if(strDialysisSchedule == 'T Th S')
                intTreatmentCount = intTThSTreatmentCount;
            else
                intTreatmentCount = (intMWFTreatmentCount > intTThSTreatmentCount) ? intMWFTreatmentCount : intTThSTreatmentCount;

            objDA.Max_Treatments_Allowed_for_Month__c = intTreatmentCount;
        }
    }

    private Integer getTreatmentCounts(Date startDate, String strPrescribedTreatment) {
        Integer daysCount = Date.daysInMonth(startDate.year(), startDate.month());
        Integer intTreatmentCount = 0;
        
        for(Integer i = 0; i < daysCount ; i++) {
            Date updatedStartDate = startDate + i;
            DateTime updatedStartDateTime = (DateTime) updatedStartDate;
            String day = updatedStartDateTime.formatGmt('EEEE');
			
            if(strPrescribedTreatment == 'M W F') {
                if(day == 'Monday')
                    intTreatmentCount += 1;
                if(day == 'Wednesday')
                    intTreatmentCount += 1;
                if(day == 'Friday')
                    intTreatmentCount += 1;
            }
            if(strPrescribedTreatment == 'T Th S') {
                if(day == 'Tuesday')
                    intTreatmentCount += 1;
                if(day == 'Thursday')
                    intTreatmentCount += 1;
                if(day == 'Saturday')
                    intTreatmentCount += 1;
            }
        }
        
        return intTreatmentCount;
    }
}